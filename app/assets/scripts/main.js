'use strict';
var isMobileScreen = function () {
    return document.body.clientWidth < 992;
};

var app = {
    init: function () {
        this.slider();
        this.calcVH();
        // this.quickcart();
    },
    slider: function() {
        $('.ul-hot-products-mobile').slick( {
            dots: true,
            arrows: false,
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 3000
        });
        if($('.slider-product').length){
            $('.slider-for').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                fade: true,
                asNavFor: '.slider-nav'
            });
            $('.slider-nav').slick({
                slidesToShow: 5,
                slidesToScroll: 1,
                asNavFor: '.slider-for',
                dots: false,
                centerMode: true,
                focusOnSelect: true,
                variableWidth: true
            });
        }
        /**/
        if ($('.slider-hero').length ) {
            $('.slider-hero').not('.slick-initialized').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                fade: true,
                dots: true,
                infinite: true,
                autoplay: true,
                autoplaySpeed: 5000
            });
        }

        /* slick only mobile */

        if ($('.ul-hot-products').length ) {
            var slick_slider = $('.ul-hot-products');
            var settings_slider = {
                dots: true,
                arrows: false,
                infinite: true,
                slidesToShow: 1,
                slidesToScroll: 1
            }
            slick_on_mobile( slick_slider, settings_slider);
            function slick_on_mobile(slider, settings){
                $(window).on('load resize', function() {
                    if ($(window).width() > 991) {
                        if (slider.hasClass('slick-initialized')) {
                            slider.slick('unslick');
                        }
                        return
                    }
                    if (!slider.hasClass('slick-initialized')) {
                        return slider.slick(settings);
                    }
                });
            };
        }

        if ($('.slider-drug-cate').length ) {
            $('.slider-drug-cate').not('.slick-initialized').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: true,
                fade: true,
                dots: true,
                infinite: true,
                autoplay: true,
                autoplaySpeed: 5000,
                nextArrow:'<button type="button" class="slick-next"></button>',
                prevArrow: '<button type="button" class="slick-prev"></button>',
                responsive: [
                    {
                        breakpoint: 992,
                        settings: {
                            arrows: false,
                            nextArrow:'',
                            prevArrow: '',
                        }
                    }
                ]
            });
        }

        if ($('.sider-pathology').length ) {
            $('.sider-pathology').not('.slick-initialized').slick({
                slidesToShow: 3,
                slidesToScroll: 3,
                dots: true,
                infinite: true,
                arrows: false,
                responsive: [
                    {
                        breakpoint: 992,
                        settings: {
                            dots: false,
                            arrows: false,
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            centerMode: false,
                            variableWidth: true
                        }
                    }
                ]
            });
        }


        // slider product
        if($('.slider-block').length){
            $('.slider-block').not('.slick-initialized').slick({
                slidesToShow: 4,
                slidesToScroll: 4,
                nextArrow:'<button type="button" class="slick-next"></button>',
                prevArrow: '<button type="button" class="slick-prev"></button>',
                dots: false,
                infinite: true,
                responsive: [
                    {
                        breakpoint: 1201,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 3,
                            arrows: false
                        }
                    },
                    {
                        breakpoint: 992,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2,
                            arrows: false
                        }
                    },
                    {
                        breakpoint: 767,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            arrows: false,
                            variableWidth: true
                        }
                    }]
            });
        }

        // slider detail
        if ($('.sider-detail').length ) {
            $('.sider-detail').not('.slick-initialized').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: false,
                infinite: true,
                arrows: false,
                centerMode: true,
                centerPadding: '140px',
                responsive: [
                    {
                        breakpoint: 768,
                        settings: {
                            centerPadding: '40px',
                            dots: false
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            centerPadding: '40px',
                            dots: false
                        }
                    }
                ]
            });
        }

    },
    closeMenuMobile: function(){
        $('.mobile-nav').removeClass('show');
        $('html, body').removeClass('open-accordion');
        $('.nav-mobile').removeClass('cancel-nav-mobile');
    },
    menuMobile: function() {
        $('.nav-mobile').click(function(){
            if (!$(this).hasClass('cancel-nav-mobile')){
                console.log('dang dong');
                $(this).addClass('cancel-nav-mobile');
                $('.mobile-nav').addClass('show');
                $('html, body').addClass('open-accordion');
            }else{
                console.log('dang mo');
                $('.mobile-nav').removeClass('show');
                $('html, body').removeClass('open-accordion');
                $('.nav-mobile').removeClass('cancel-nav-mobile');
            }
        });
        $('.cancel-nav-mobile,.overlay-mobile-nav').click(function(){
            app.closeMenuMobile();
        });
        $('.card.no-sub-nav .card-header a').click(function(){
            app.closeMenuMobile();
        });
        $('.ul-products li').click(function(){
            app.closeMenuMobile();
        });
        app.accountDropdown();
    },
    accountDropdown: function(){
        $('#dropdownAccountHeader').on('show.bs.dropdown', function () {
            $('.mobile-nav').removeClass('show');
            $('.nav-mobile').removeClass('cancel-nav-mobile');
            $('html, body').addClass('open-accordion');
        });
        $('#dropdownAccountHeader').on('hide.bs.dropdown', function () {
            $('html, body').removeClass('open-accordion');
        })
    },
    slideToUnlockBtn : function(id, messages){
        console.log('id: ', id);
        if ($(id).length ) {
            $(id).slideToUnlock({
                lockText: messages,
                unlockText: ''
            })
        }
    },
    loginModalShow: function() {
        if ($('#loginModal').length) {
            $('#loginModal').on('shown.bs.modal', function (e) {
                app.slideToUnlockBtn('#sliderSendSms', 'trượt để nhận SMS');
            });
        }
    },
    resetPasswordModalShow: function(){
        if ($('#forgotPassword').length) {
            $('#forgotPassword').on('shown.bs.modal', function (e) {
                app.slideToUnlockBtn('#sliderSendNewPassword','trượt để nhận mật khẩu mới');
            });
        }
    },
    smsSliderShow: function() {
        if ($('.sms-slider').length) {
            app.slideToUnlockBtn('#sliderSendSms-gen', 'trượt để nhận SMS');
        }
        if ($('.code-slider').length) {
            app.slideToUnlockBtn('#sliderSendCode-gen', 'trượt để nhận mã xác thực');
        }
    },
    calcVH: function () {
        // First we get the viewport height and we multiple it by 1% to get a value for a vh unit
        var vh = window.innerHeight * 0.01;
        // Then we set the value in the --vh custom property to the root of the document
        document.documentElement.style.setProperty('--vh', `${vh}px`);
    },
    preventClickInsideDropdown: function(){
        if ($('.dropdown-menu').length ) {
            $('.dropdown-menu.mega-dropdown-menu').on('click', function(event){
                // The event won't be propagated up to the document NODE and
                // therefore delegated events won't be fired
                console.log('prevent');
                event.stopPropagation();
            });
        }
    },
    preventScrollAccount: function(){
        if ($('#dropdownAccountDropdown').length ) {
            $('#dropdownAccountDropdown').on('show.bs.dropdown', function () {
                $('body').addClass('fixedPosition');
                app.closeMenuMobile();
            });
            $('#dropdownAccountDropdown').on('hide.bs.dropdown', function () {
                $('body').removeClass('fixedPosition');
            })
        }
    },
    quickView: function(){
        if ($('#quickView').length ) {
            $('#quickView').on('shown.bs.modal', function () {
                console.log('show modal');
                $('.slider-for').slick({
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false,
                    fade: true,
                    asNavFor: '.slider-nav'
                });
                $('.slider-nav').slick({
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    asNavFor: '.slider-for',
                    dots: false,
                    arrows: false,
                    focusOnSelect: true
                });
            });
        }
    },
    quickcart: function(){
        if ($('#quickCartDropdown').length) {
            $('#quickCartDropdown').on('shown.bs.dropdown', function () {
                $('.prefectScroll').jScrollPane();
            });
        }

    },
    backToTop: function(){
        $(window).scroll(function(){
            if ($(this).scrollTop() > 100) {
                $('.back-to-top').fadeIn();
            } else {
                $('.back-to-top').fadeOut();
            }
        });
        $('.back-to-top').click(function(){
            $('html, body').animate({ scrollTop: 0 }, 600);
            return false;
        });

    },
    endOpenModal: function(){
        $('body').on('show.bs.modal', '.modal', function () {
            $('.modal:visible').removeClass('fade').modal('hide').addClass('fade');
        });

        $('#regToLogin').on('click',function(){
            // $('#regModal').modal('hide');
            $('.modal').modal('hide');
            setTimeout(() => {
                $('#loginModal').modal('show');
            }, 500);
        });
        $('#loginToReg').on('click',function(){
            // $('#loginModal').modal('hide');
            $('.modal').modal('hide');
            setTimeout(() => {
                $('#regModal').modal('show');
            }, 500);
        });

        $('#doneReg').on('click',function(){
            // $('#regModal').modal('hide');
            $('.modal').modal('hide');
            setTimeout(() => {
                $('#successReg').modal('show');
            }, 500);
        });

        $('#loginToForgotPass').on('click',function(){
            $('.modal').modal('hide');
            setTimeout(() => {
                $('#forgotPassword').modal('show');
            }, 500);
        });

        $('#resetPassToLogin').on('click',function(){
            $('.modal').modal('hide');
            setTimeout(() => {
                $('#loginModal').modal('show');
            }, 500);
        });
        $('#confirm-tel').on('click',function(){
            $('.modal').modal('hide');
            setTimeout(() => {
                $('#confirmTel').modal('show');
            }, 500);
        });
    },
    openSearchDesktop: function(){
        $('#seach-desktop').click(function(){
            $('.main-nav-and-search').toggleClass('expand-full-search');
        })
    },
    editable: function(){
        // toggle edit email - my account
        var ediable = 0;
        $('.edit-email').click(function(){
            var editParent = $(this).parent();

            if(!ediable){
                console.log('1----');
                editParent.removeClass('disabled-input');
                editParent.find('.form-control').removeAttr('disabled');
            }else{
                console.log('0');
                editParent.addClass('disabled-input');
                editParent.find('.form-control').attr('disabled','disabled');
            }
            ediable != giediable;
        })
    }
};

$(document).ready(function () {
    app.init();
    app.menuMobile();
    app.loginModalShow();
    app.resetPasswordModalShow();
    app.preventScrollAccount();
    app.quickView();
    app.preventClickInsideDropdown();
    app.quickcart();
    app.backToTop();
    // call modal
    app.endOpenModal();
    app.smsSliderShow();
    app.openSearchDesktop();
    app.accountDropdown();
    app.editable();


    var resizeId;
    $(window).resize(function () {
        clearTimeout(resizeId);
        resizeId = setTimeout(function () {
            app.slider();
            slick_on_mobile( slick_slider, settings_slider);

            app.calcVH();
            app.quickView();
        });
    });

    //
    $('.collapse').on('shown.bs.collapse', function () {
        $(this).prev().addClass('active');
    });

    $('.collapse').on('hidden.bs.collapse', function () {
        $(this).prev().removeClass('active');

    });

    // order-payment : payment method
    $('input[name=\'payment-method\']').change(function(e){
        var selected_payment = $('input[name=\'payment-method\']:checked').attr('data-external');
        $('.show-external-payment').removeClass('show-external-payment');
        $('.' + selected_payment).addClass('show-external-payment');
    });

    $('input[name=\'bill\']').change(function(e){
        var selected_bill = $('input[name=\'bill\']:checked').attr('data-external');
        $('.show-external-vat').removeClass('show-external-vat');
        $('.' + selected_bill).addClass('show-external-vat');
    });

    // sk-detail bai viet and sk-search-tag
    if ($('.is-sticky').length ) {
        $('.is-sticky').sticksy({
            topSpacing: 50
        });
    }

    // cau hoi
    function toggleQuestion(){
        var $itemQuestion = $('.ul-list-text-question > li');
        if ($itemQuestion.length) {
            $('.ul-list-text-question > li').first().addClass( 'active' );
            $('.ul-list-text-question > li .toggle-answer').click(function(){
                $(this).parent().parent().parent().toggleClass('active');
            })
        }
    }
    toggleQuestion();

    // Sync modal
    $('#cartSynModal').on('shown.bs.modal', function () {
        $('.prefectScroll').jScrollPane();
    })


    // select header
    $('.js-select-nn').select2({
        minimumResultsForSearch: Infinity
    });


    //
    $('.style-frequently-question').on('shown.bs.collapse', function () {
        $(this).prev().addClass('active');
    });

    $('.style-frequently-question').on('hidden.bs.collapse', function () {
        $(this).prev().removeClass('active');

    });
    // add to cart
    $('.item-product:not(\'.out-of-stock\') .btn-add-to-cart').click(function(){
        console.log('click');
        var productCard = $(this).parent().parent();
        var position = productCard.offset();
        var productImg = $(productCard).find('.img-product img');


        $('body').append('<div class="floating-cart"></div>');
        var cart = $('div.floating-cart');
        productImg.clone().appendTo(cart);
        $(cart).css({
            'top' : position.top + 'px',
            'left' : position.left + 'px',
            'opacity': '0.5'
        }).fadeIn('slow').addClass('moveToCart');
        setTimeout(function(){
            $('div.floating-cart').remove();
        }, 500);
    });
    // add to cart quickview modal
    $('.cta-quick-view .btn-modal-add-cart').click(function(){
        console.log('click');
        var productCard = $(this).parents('.quick-view-ct');
        var position = productCard.offset();
        var productImg = $(productCard).find('.carousel-product > img');


        $('body').append('<div class="floating-cart floating-cart-quick-view"></div>');
        var cart = $('div.floating-cart');
        productImg.clone().appendTo(cart);
        $(cart).css({
            'top' : position.top + 'px',
            'left' : position.left + 'px',
            'opacity': '0.5',
            'zIndex':'3000'
        }).fadeIn('slow').addClass('moveToCart');
        setTimeout(function(){
            $('div.floating-cart').remove();
        }, 500);
    });
    // add to card detail product
    $('.btn-add-to-cart-detail').click(function(){
        var position = $(this).offset();


        $('body').append('<div class="floating-cart floating-cart-detail"></div>');
        var cart = $('div.floating-cart');
        $(this).clone().appendTo(cart);
        $(cart).css({
            'top' : position.top + 'px',
            'left' : position.left + 'px',
            'opacity': '0.5',
            'zIndex':'3000'
        }).fadeIn('slow').addClass('moveToCart');
        //
        setTimeout(function(){
            $('div.floating-cart').remove();
        }, 500);
    });
    //
    // $('#example-getting-started').multiselect({
    //     nonSelectedText: 'Chưa chọn',
    //     nSelectedText: 'triệu chứng',
    //     allSelectedText: 'Tất cả',
    // });
    // save to my favorite
    $('.save-my-favorite').click(function(){
        $(this).toggleClass('saved');
    })

    // view more mobile - detail promotion
    $('.view-more-detail-link').click(function(){
        $('.section-detail-promotion').toggleClass('show');
        if($('.section-detail-promotion').hasClass('show')){
            $(this).text('Thu gọn');
        }else{
            $(this).text('Xem thêm');
        }

    })


});

$(window).on('load', function () {

});

